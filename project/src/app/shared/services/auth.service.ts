import { User } from './../models/user';
import { Injectable } from '@angular/core';
import { HttpClient, HttpErrorResponse } from '@angular/common/http';
import { throwError, BehaviorSubject } from 'rxjs';
import { catchError } from 'rxjs/operators';

@Injectable({
  providedIn: 'root'
})
export class AuthService {
  private isLoggedIn: BehaviorSubject<Object> = new BehaviorSubject<Object>(null);
  userInfo = this.isLoggedIn.asObservable();

  constructor(private http: HttpClient) { }

  register(user: User) {
    return this.http.post<User>('/user/register', user)
                .pipe(
                  catchError((err: HttpErrorResponse) => {
                    return throwError(err.error);
                  })
                );
  }

  login(user: {email: String, password: String}) {
    return this.http.post<any>('/user/login', user)
                .pipe(
                  catchError((err: HttpErrorResponse) => {
                    return throwError(err.error);
                  })
                );
  }

  setLoggedIn(userInfo: Object | null) {
    this.isLoggedIn.next(userInfo);
  }

}
