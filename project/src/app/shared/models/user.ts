export interface User {
  email: String;
  fullName: String;
  gender: String;
  nickName: String;
  password: String;
  re_password: String;
  secret: String;
}
