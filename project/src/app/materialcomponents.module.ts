import { NgModule } from '@angular/core';
import {
  MatDividerModule,
  MatCardModule,
  MatFormFieldModule,
  MatInputModule,
  MatSelectModule,
  MatCheckboxModule,
  MatButtonModule,
  MatProgressSpinnerModule,
  MatDialogModule
} from '@angular/material';
const components = [
  MatDividerModule,
  MatCardModule,
  MatFormFieldModule,
  MatInputModule,
  MatSelectModule,
  MatCheckboxModule,
  MatButtonModule,
  MatProgressSpinnerModule,
  MatDialogModule
];

@NgModule({
  imports: components,
  exports: components
})
export class MaterialcomponentsModule { }
