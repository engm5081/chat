import { Component, OnInit } from '@angular/core';
import { ActivatedRoute } from '@angular/router';
import { NgForm } from '@angular/forms';
import { AuthService } from '../shared/services/auth.service';

@Component({
  selector: 'chat-login',
  templateUrl: './login.component.html',
  styleUrls: ['./login.component.scss']
})
export class LoginComponent implements OnInit {
  email;
  errors: String[] = [];
  loading: Boolean = false;
  constructor(private route: ActivatedRoute, private auth: AuthService) { }

  ngOnInit() {
      this.email = this.route.snapshot.queryParams['email'] || '';
  }

  onSubmit({valid, value}: NgForm) {
    if (valid) {
      this.auth.login(value)
          .subscribe(
            info => {
              for (const key in info) {
                if (typeof info[key] === 'object' && info[key] instanceof Array) {
                  localStorage.setItem(key, JSON.stringify(info[key]));
                } else {
                  localStorage.setItem(key, info[key]);
                }
              }
              this.auth.setLoggedIn(info);
            },
            ({errors}) => {
              this.errors = errors;
            }
          );
    }
  }

}
