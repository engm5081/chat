import { Component, ViewChild, Inject, ViewEncapsulation } from '@angular/core';
import { NgForm } from '@angular/forms';
import { AuthService } from '../shared/services/auth.service';
import { Router } from '@angular/router';
import { MatDialog, MAT_DIALOG_DATA, MatDialogRef } from '@angular/material';

@Component({
  selector: 'chat-register',
  templateUrl: './register.component.html',
  styleUrls: ['./register.component.scss']
})
export class RegisterComponent {
  errors: String[] = [];
  @ViewChild('f') form: NgForm;
  loading: Boolean = false;
  isChecked: Boolean;

  constructor(private auth: AuthService, private router: Router, private dialog: MatDialog) { }

  /* start dialog */
  openDialog(): void {
    const dialogRef = this.dialog.open(RegisterDialogComponent, {
      data: true
    });

    dialogRef.afterClosed().subscribe(result => {
      result = result || false;
      this.isChecked = result;
    });
  }
  /* end dialog */


  onSubmit({valid, value}: NgForm) {
    if (valid && value.password === value.re_password) {
      this.loading = true;
      this.auth.register(value)
          .subscribe(
            user => {
              this.loading = false;
              this.router.navigate(['/login'], {queryParams: {email: user.email}});
            },
            ({errors}) => {
              this.loading = false;
              this.errors = errors;
            }
          );
    }
    if (value.password !== value.re_password) {
      this.errors = ['Passwords are not match'];
      this.form.setValue({
        ...value,
        password: '',
        re_password: ''
      });
    }
  }

}

/* Register Dialog */
@Component({
  selector: 'chat-registerdialog',
  templateUrl: 'registerdialog.html',
  styleUrls: ['./registerdialog.component.scss'],
  encapsulation: ViewEncapsulation.None
})
export class RegisterDialogComponent {

  constructor(
    public dialogRef: MatDialogRef<RegisterDialogComponent>,
    @Inject(MAT_DIALOG_DATA) public data: any) {}

  onNoClick(): void {
    this.dialogRef.close();
  }

}
