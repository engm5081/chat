import { Component, OnInit } from '@angular/core';
import { BreakpointObserver, Breakpoints, BreakpointState } from '@angular/cdk/layout';
import { Observable } from 'rxjs';
import { map } from 'rxjs/operators';
import { AuthService } from '../shared/services/auth.service';

@Component({
  selector: 'chat-layout',
  templateUrl: './layout.component.html',
  styleUrls: ['./layout.component.css']
})
export class LayoutComponent implements OnInit {
  userInfo: Object;
  constructor(private breakpointObserver: BreakpointObserver, private auth: AuthService) {}

  links: {path: String, to: String}[] = [
    {path: 'Home', to: '/'},
    {path: 'Login', to: '/login'},
    {path: 'Register', to: '/register'}
  ];

  isHandset$: Observable<boolean> = this.breakpointObserver.observe(Breakpoints.Handset)
    .pipe(
      map(result => result.matches)
    );

    ngOnInit() {
      this.auth.userInfo.subscribe(
        userInfo => {
          console.log('userInfo:', userInfo);
          this.userInfo = userInfo;
        }
      );
    }

    logout() {
      localStorage.clear();
      this.auth.setLoggedIn(null);
    }


  }
