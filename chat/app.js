const express = require("express");
const app = express();
const socketIO = require("socket.io");
const server = require("http").createServer(app);
const bodyParser = require("body-parser");
const mongoose = require("mongoose");
const path = require("path");
const { port, mongoUrl } = require("./configs/config");
const io = socketIO(server);

// mongoose connect & config
// mongoose.set("useCreateIndex", true);
mongoose.Promise = global.Promise;
mongoose.connect(mongoUrl, {useMongoClient: true}, () => console.log("DB connecterd on:", mongoUrl));
require("./models/user.model"); // register user model(Schema)

// static folder
app.use(express.static("public"));

// body parser middleware
app.use(bodyParser.json());
app.use(bodyParser.urlencoded({extended: false}));

// app routes
app.get("*", (_, res) => res.sendFile(path.join(__dirname, "public/index.html")));
const user = require("./routes/user.routes");
app.use("/user", user);

// import chat files

server.listen(port, () => {
    console.log(`server started on port: ${port}`)
    require("./chat/chat");
});

module.exports = {app, io, mongoose};