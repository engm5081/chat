const router = require("express").Router();
const mongoose = require("mongoose");
const isEmail = /^(([^<>()\[\]\\.,;:\s@"]+(\.[^<>()\[\]\\.,;:\s@"]+)*)|(".+"))@((\[[0-9]{1,3}\.[0-9]{1,3}\.[0-9]{1,3}\.[0-9]{1,3}\])|(([a-zA-Z\-0-9]+\.)+[a-zA-Z]{2,}))$/;
const User = mongoose.model("user");
const jwt = require("jsonwebtoken");
const { jwtSecret } = require("../configs/config");
const message = require("../configs/message");
const { genRandomPass } = require("../configs/helpers");
const bcrypt = require("bcryptjs");

// register new user
router.post("/register", (req, res) => {
    const errors = [];
    let {email, fullName, nickName, password, re_password, secret, gender} = req.body;
    
    if (email) {
        email = String(email).trim();
        if (!isEmail.test(email.toLocaleLowerCase())) errors.push("Eamil is not valid");
    } else errors.push("Email is required");
        

    if (fullName) {
        fullName = String(fullName).trim();
        if (fullName.length === 0) errors.push("Full name is not valid");
    } else errors.push("Full name is required");

    if (nickName) {
        nickName = String(nickName).trim();
        if (nickName.length === 0) errors.push("Nick name is not valid");
    } else errors.push("Nick name is required");

    if (password === re_password) {
        password = String(password).trim();
        if (password.length < 6) errors.push("Password should be at least 6 chars");
    } else errors.push("Passwords are not match");

    if (secret) {
        secret = String(secret).trim();
        if (secret.length < 6) errors.push("secret should be at least 6 chars");
    } else errors.push("Secret is required");

    if (gender) {
        gender = String(gender).trim();
        if (gender !== "male" && gender !== "female") errors.push("invalid value for gender");
    } else errors.push("Gender is required"); 

    if (errors.length > 0) return res.status(400).json({errors});
     
    
    new User({
        email,
        fullName,
        nickName,
        password: bcrypt.hashSync(password, 8),
        secret,
        gender
    }).save()
    .then(user => {
        res.json({email: user.email});
    })
    .catch(err => {
        const { errmsg } = err;
        if (String(errmsg).indexOf("duplicate") > -1 && String(errmsg).indexOf(email)) return res.status(400).json({errors: ["User already found"]});
        res.status(400).json({errors: ["Something went wrong"]});
    });

});

// login user
router.post("/login", (req, res) => {
    let {email, password} = req.body;
    email = email.trim();
    password = password.trim();
    User.findOne({email}, (err, user) => {
        if (err) return res.status(400).json({errors: ["Something went wrong"]});
        if (!user) return res.status(400).json({errors: ["Invalid account please checkout email & password"]});
        bcrypt.compare(password, user.password, (err2, isMatch) => {
            if (err2) return res.status(400).json({errors: ["Something went wrong"]});
            if (!isMatch) return res.status(400).json({errors: ["Invalid account please checkout email & password"]});
            const token = jwt.sign({ id: user._id, date: new Date().getTime()}, jwtSecret, {
                expiresIn: 7200
            });
            console.log(user.rooms);
            res.json({
                token,
                expiresIn: 7200,
                email,
                fullName: user.fullName,
                gender: user.gender,
                nickName: user.nickName,
                avater: user.avater,
                rooms: user.rooms
            })
        });
    })
});

// refresh user token
router.post("/refresh", (req, res) => {
    let {email, token} = req.body;
    email = String(email).trim();
    let id;
    try {
        id = jwt.decode(token).id;
    } catch (err) {
        return res.status(400).json({errors: ["Invalid token or email"]});
    }
    User.findById(id, (err, user) => {
        if (err) return res.status(400).json({errors: ["Something went wrong"]});
        if (!user || user.email !== email) return res.status(400).json({errors: ["Invalid token or email"]});
        if (user.email === email) {
            const newToken = jwt.sign({ id: user._id, date: new Date().getTime()}, jwtSecret, {
                expiresIn: 7200
            });
            res.json({
                token: newToken,
                expiresIn: 7200
            });
        }
    })
});

// reset user password
router.post("/forgotpass", (req, res) => {
    let {email, secret} = req.body;
    email = String(email).trim();
    secret = String(secret).trim();
    User.findOne({email}, (err, user) => {
        if (err) return res.status(400).json({errors: ["Something went wrong"]});
        if (!user) return res.status(400).json({errors: ["Email or secret is not valid"]});
        if (email !== user.email || secret !== user.secret) return res.status(400).json({errors: ["Email or secret is not valid"]});
        const newPassword = genRandomPass();
        User.findOneAndUpdate({email}, {$set: {password: bcrypt.hashSync(newPassword, 8)}}, {new: true}, (err2, newUser) => {
            if (err2) return res.status(400).json({errors: ["Something went wrong"]});
            if (!newUser) return res.status(400).json({errors: ["Email or secret is not valid"]});
            message({
                to: email,
                subject: `Reset ${user.nickName}'s password`,
                html: `
                    Hello sir ${user.fullName},
                        your account's password was changed to be <strong>${newPassword}</strong> 
                        please make sure to save it some safe notes.
    
                    Sender: Mean Stack Chat Creator.
                `
            }, (isSent)  => {
                if (isSent) return res.json({
                    msg: `A new password was sent to ${email}` 
                });
                res.status(400).json({errors: ["Something went wrong"]});
            });
        });

    });
});

/* 
 * :TODO 
 * add test cases for changepass router
*/
// change password
router.put("/changepass", (req, res) => {
    let {email, secret, newPassword1, newPassword2} = req.body;
    const errors = [];
    if (!email) errors.push("Email is required");
    if (!secret) errors.push("Secret is required");
    if (!newPassword1) errors.push("New password is required");
    if (newPassword1 !== newPassword2) errors.push("Passwords are not match");
    if (errors.length > 0) return res.json({errors});
    email = email.trim();
    secret = secret.trim();
    newPassword1 = newPassword1.trim();
    newPassword2 = newPassword2.trim();
    User.findOne({email}, (err, user) => {
        if (err) return res.json({errors: ["Something went wrong"]});
        if (!user) return res.json({errors: ["User not found"]});
        if (user.secret !== secret) return res.json({errors: ["Incorrect secret"]});
        User.findOneAndUpdate({email}, {$set: {password: bcrypt.hashSync(newPassword1, 8)}}, {new: true}, (err2, newUser) => {
            if (err2 || !newUser) return res.json({errors: ["Something went wrong"]});
            res.json({
                msg: "Password successfully changed"
            })
        });
    });
});

module.exports = router;