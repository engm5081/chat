const helpers = {};

helpers.genRandomPass = () => {
    let pass = '',
        rand;
    const chars = 'abcdefghijklmnopqrstuvwxyzABCDEFGHIJKLMNOPQRSTUVWXYZ1234567890',
          len = chars.length;
    for (let i = 0; i < 10; i++) {
        rand = Math.floor(Math.random() * len);
        pass += chars[rand];
    }
    return pass;
};

module.exports = helpers;