const nodemailer = require("nodemailer");
const { nodeMailerAuth } = require("./config");

module.exports = (data, callback) => {
    const transporter = nodemailer.createTransport({
        service: 'gmail',
        auth: nodeMailerAuth
       });

    const mailOptions = {
        from: 'Chat Team',
        ...data
      };

    transporter.sendMail(mailOptions, function (err, info) {
        if (err || !info) return callback(false);
        callback(true);
    });
};