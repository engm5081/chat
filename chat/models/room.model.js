const mongoose = require("mongoose");
const Schema = mongoose.Schema;

const room = new Schema({
    name: {
        required: false,
        type: String
    },
    msgs: {
        type: Array,
        required: false,
        default: []
    },
    Date: {
        type: Date,
        default: new Date().getTime()
    }
});

mongoose.model("room", room);

/* - msg
 * contains
 * {
 *  sender: "name",
 *  content: "asdas"
 *  date: new Date().getTime()
 * }
*/