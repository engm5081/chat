const mongoose = require("mongoose");
const Schema = mongoose.Schema;

const user = new Schema({
    email: {
        type: String,
        required: true,
        trim: true,
        unique: true
    },
    fullName: {
        type: String,
        required: true,
        trim: true
    },
    gender: {
        type: String,
        required: true
    },
    nickName: {
        type: String,
        required: true,
        trim: true
    },
    secret: {
        type: String,
        required: true,
        trim: true
    },
    avater: {
        type: String,
        trim: true,
        default: "http://placehold.it/80x80"
    },
    password: {
        type: String,
        trim: true,
        required: true
    },
    rooms: {
        type: Array,
        required: false,
        default: []
    },
    date: {
        type: Date,
        default: new Date().getTime()
    }
});

mongoose.model("user", user);

/* - room
 * contains
 * {
 *  _id,
 *  name: [person1, person2],
 *  images: [img1, img2],
 *  lastMsg: "msg",
 *  lastSender: "person1 or person2",
 *  sentTime: Date
 * }
*/