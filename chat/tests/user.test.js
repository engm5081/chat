const expect = require("expect");
const request = require("supertest");
const {app, mongoose} = require("../app");
const bcrypt = require("bcryptjs");
const axios = require("axios");
const {port} = require("../configs/config");
const User = mongoose.model("user");

beforeEach(() => {
    const users = [
        {
            "email": "memo1@yahoo.com",
            "fullName": "Mohamed Elmdary",
            "nickName": "aa",
            "password": bcrypt.hashSync("123456", 8),
            "secret": "test12"
        }
    ];
    User.deleteMany({})
        .then(users => {})
        .catch(err => {});

    User.insertMany(users)
        .then(users => {})
        .catch(err => {});
});

describe("user.routes.js", () => {

    describe("handle user register", () => {
        it("should return the correct user", done => {
            const user = {
                "email": "memo@yahoo.com  ",
                "fullName": "Mohamed Elmdary ",
                "nickName": "          aa ",
                "password": "123456",
                "re_password": "123456",
                "secret": "123456"
            };
            request(app)
                .post("/user/register")
                .send(user)
                .expect(200)
                .expect(res => {
                    const {email, fullName, nickName, password} = res.body.user;
                    expect(email).toBeA("string").toEqual(user.email.trim());
                    expect(fullName).toBeA("string").toEqual(user.fullName.trim());
                    expect(nickName).toBeA("string").toEqual(user.nickName.trim());
                    bcrypt.compare(user.password, password, function(err, res) {
                        expect(res).toBeA("boolean").toBe(true);
                    });
                })
                .end(done);
        });
    
        it("should not save user", done => {
            const user = {
                "email": "memo1@yahoo.com",
                "fullName": "Mohamed Elmdary",
                "nickName": "aa",
                "password": "123456",
                "re_password": "123456",
                "secret": "123456"
            };
            request(app)
                .post("/user/register")
                .send(user)
                .expect(400)
                .expect(res => {
                    expect(res.body.user).toBe(undefined);
                    expect(res.body.errors.length).toBe(1);
                    expect(res.body.errors[0]).toBeA("string").toBe("User already found");
                })
                .end(done);
        });
    
        it("should return some errors", done => {
            const user = {
                "email": "  ",
                "fullName": "  ",
                "nickName": "  ",
                "password": "   ",
                "re_password": "123456"
            };
            request(app)
                .post("/user/register")
                .send(user)
                .expect(400)
                .expect(res => {
                    expect(res.body.user).toBe(undefined);
                    expect(res.body.errors.length).toBe(5);
                    expect(res.body.errors).toEqual([ 'Eamil is not valid', 'Full name is not valid', 'Nick name is not valid', 'Passwords are not match', 'Secret is required'])
                })
                .end(done);
        });

    });

    describe("handle user login", () => {
        
        it("should return token and expiresIn", done => {
            const user = {
                "email": "memo1@yahoo.com",
                "password": "123456"
            };
            request(app)
                .post("/user/login")
                .send(user)
                .expect(200)
                .expect(res => {
                    expect(res.body.token).toBeA('string').toExist();
                    expect(res.body.expiresIn).toBeA('number').toBe(7200);
                })
                .end(done);
        });

        it("should not match password", done => {
            const user = {
                "email": "memo1@yahoo.com",
                "password": "1234567"
            };
            request(app)
                .post("/user/login")
                .send(user)
                .expect(400)
                .expect(res => {
                    expect(res.body.errors.length).toBe(1);
                    expect(res.body.errors[0]).toBeA('string').toBe("Invalid account please checkout email & password");
                })
                .end(done);
        });

        it("should not find email", done => {
            const user = {
                "email": "memo2@yahoo.com",
                "password": "123456"
            };
            request(app)
                .post("/user/login")
                .send(user)
                .expect(400)
                .expect(res => {
                    expect(res.body.errors.length).toBe(1);
                    expect(res.body.errors[0]).toBeA('string').toBe("Invalid account please checkout email & password");
                })
                .end(done);
        });

    });

    describe("handle user refresh token", () => {

        it("should return new token", done => {
            const user = {
                email: "memo1@yahoo.com",
                password: "123456"
            }
            axios.post(`http://localhost:${port}/user/login`, user)
            .then(({data}) => {
                const {token} = data;
                request(app)
                    .post("/user/refresh")
                    .send({
                        token,
                        email: user.email
                    })
                    .expect(200)
                    .expect(res => {
                        expect(res.body.token).toNotEqual(token);
                        expect(res.body.token).toBeA('string').toExist();
                        expect(res.body.expiresIn).toBeA('number').toBe(7200);
                    })
                    .end(done);
            });
        });

        it("should return some errors for wrong email", done => {
            const user = {
                email: "memo1@yahoo.com",
                password: "123456"
            }
            axios.post(`http://localhost:${port}/user/login`, user)
            .then(({data}) => {
                const {token} = data;
                request(app)
                    .post("/user/refresh")
                    .send({
                        token,
                        email: "memo2@yahoo.com"
                    })
                    .expect(400)
                    .expect(res => {
                        expect(res.body.errors.length).toBe(1);
                        expect(res.body.errors[0]).toBeA('string').toBe("Invalid token or email");
                    })
                    .end(done);
            });
        });


        it("should return some errors for wrong token", done => {
            const user = {
                email: "memo1@yahoo.com",
                password: "123456"
            }
            axios.post(`http://localhost:${port}/user/login`, user)
            .then(({data}) => {
                const {token} = data;
                request(app)
                    .post("/user/refresh")
                    .send({
                        token: "invalid.token.test",
                        email: user.email
                    })
                    .expect(400)
                    .expect(res => {
                        expect(res.body.errors.length).toBe(1);
                        expect(res.body.errors[0]).toBeA('string').toBe("Invalid token or email");
                    })
                    .end(done);
            });
        });

    });

    describe("handle user forgot password", () => {

        it("should change password correctly", done => {
            const user = {
                "email": "memo1@yahoo.com",
                "secret": "test12"
            };
            request(app)
                .post("/user/forgotpass")
                .send(user)
                .expect(200)
                .expect(res => {
                    expect(res.body.msg).toBeA('string').toBe(`A new password was sent to ${user.email}`);
                })
                .end(done);
        });

        it("shouldn't change user password wrong secret", done => {
            const user = {
                "email": "memo1@yahoo.com",
                "secret": "test123"
            };
            request(app)
                .post("/user/forgotpass")
                .send(user)
                .expect(400)
                .expect(res => {
                    expect(res.body.errors.length).toBe(1);
                    expect(res.body.errors[0]).toBeA('string').toBe("Email or secret is not valid");
                })
                .end(done);
        });

        it("shouldn't change user password wrong email", done => {
            const user = {
                "email": "memo10@yahoo.com",
                "secret": "test12"
            };
            request(app)
                .post("/user/forgotpass")
                .send(user)
                .expect(400)
                .expect(res => {
                    expect(res.body.errors.length).toBe(1);
                    expect(res.body.errors[0]).toBeA('string').toBe("Email or secret is not valid");
                })
                .end(done);
        });

    });

});