const expect = require("expect");
const { genRandomPass } = require("../configs/helpers");

describe("helpers.test.js", () => {
    
    describe("generate random password", () => {
        
        it("should generate random password", () => {
            expect(genRandomPass()).toBeA("string");
            expect(genRandomPass().length).toBe(10);
        });

        it("parameters shouldn't affect it", () => {
            expect(genRandomPass(25)).toBeA("string");
            expect(genRandomPass(25).length).toBe(10);
        });

    });

});